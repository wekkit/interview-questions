# Simple code exercise with test.

The interview strategy goes on four main points - culture fit, architecture knowledge, relevance of past experience and programming skills. they look for at least 3 out of 4 of these in a candidate for serious consideration

### This repo contains the solution for the following code exercise:

Create a module with an exported function that takes in this input and returns the expected output. Create a test file that tests your function with this input/output using mocha and chai.

`const input = [1, 2, 3, 4, 5]`
`const expectedOutput = [50, 40, 30, 20, 10]`

The module in question is found in `module.js`. To run the tests using mocha, run `npm install` to install mocha and run `npm run test` to run the `module.spec.js` test file.

### Additional (verbal) technical questions may include examples such as:

- Explain the concepts of prototypes in javascript.
- What is an API? Explain your understanding of a web API.
- Explain the concept of JWT.
- How does the event loop work in Javascript?
- What is express middleware?
- How do you deep clone an object
- What is React Flux?
- Is it possible to create a resource using a GET request? If so, how would you do so?

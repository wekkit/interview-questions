const assert = require('assert')
const func = require('./module')
const input = [1, 2, 3, 4, 5]
const expectedOutput = [50, 40, 30, 20, 10]

describe('testing module function', () => {
  it('it should take in the input and produce the expected output', () => {
    assert.deepEqual(func(input), expectedOutput)
  })
})

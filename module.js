module.exports = function func(arr) {
  return arr.reverse().map(x => x * 10)
}
